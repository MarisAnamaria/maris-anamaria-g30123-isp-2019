package maris.anamaria.lab8.ex1;

public class Test {
	public static void main(String[] args){
        Persoana p1 = new Persoana("A", 10);
        ContBancar c = new ContBancar(p1, 0.1);

        c.detalii();
        c.depunere(600);
        c.actualizare();
        c.detalii();

        ManagerConturi mc = new ManagerConturi();

        Persoana a = new Persoana("Ana",34);
        Persoana b = new Persoana("Corina",27);
        Persoana e = new Persoana("Bea",21);

        mc.creareCont(a, 0.05);
        mc.creareCont(b, 0.06);
        mc.creareCont(e, 0.04);

        mc.afiseaza();
    }
}
