package maris.anamaria.lab4.ex2;

public class TestAuthor {
	public static void main(String[] args) {
        Author a = new Author("Maris Anamaria", "anamaria.maris@gmail.com", 'f');
        a.setEmail("ana.maris@gmail.com");
        System.out.println(a.getEmail() + " " + a.getName() + " " + a.toString());
    }
}
