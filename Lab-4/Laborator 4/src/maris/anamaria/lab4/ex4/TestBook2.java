package maris.anamaria.lab4.ex4;

import java.util.Arrays;
import maris.anamaria.lab4.ex2.Author;

public class TestBook2 {
	public static void main(String[] args) {
        Author[] aut = new Author[3];
        aut[0] = new Author("Dan Brown", "dbrown.users@mail.com", 'm');
        aut[1] = new Author("Jojo Moyes", "jojom@yahoo.com", 'f');
        aut[2] = new Author("Michelle Obama", "mobama@gmail.com", 'f');
        Book b = new Book("Origini", aut, 70, 4);
        Book b1 = new Book("Un bilet pentru Paris", aut, 40, 12);
        Book b2 = new Book("Povestea mea", aut, 50, 9);
        System.out.println(b.toString() + "\n");
        b.setPrice(70.32);
        b.setQtyInStock(42);
        b.printAuthors();
        b1.setPrice(40.23);
        b1.setQtyInStock(19);
        b1.printAuthors();
        b2.setPrice(50.22);
        b2.setQtyInStock(23);
        b2.printAuthors();
        System.out.println(Arrays.toString(b.getAuthor()) + " " + b.getName() + " " + b.getPrice() + " " + b.getQtyInStock());
        System.out.println(Arrays.toString(b1.getAuthor()) + " " + b1.getName() + " " + b1.getPrice() + " " + b1.getQtyInStock());
        System.out.println(Arrays.toString(b2.getAuthor()) + " " + b2.getName() + " " + b2.getPrice() + " " + b2.getQtyInStock());
    }
}
