package maris.anamaria.lab4.ex1;

public class TestCircle {
	public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(5.3);
        System.out.println(c1.getRadius() + " " + c1.getArea());
        System.out.println(c2.getRadius() + " " + c2.getArea());

    }
}
