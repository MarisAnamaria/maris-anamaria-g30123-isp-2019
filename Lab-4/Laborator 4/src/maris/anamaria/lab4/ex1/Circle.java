package maris.anamaria.lab4.ex1;

public class Circle {
	private String color;
    private double radius;

    public Circle() {
    	this.color = "red";
    	this.radius = 1.0; 
    }

    public Circle(double radius) {
        this();
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return 3.14 * radius * radius;
    }

}
