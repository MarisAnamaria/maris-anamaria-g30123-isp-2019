package maris.anamaria.lab4.ex3;

import maris.anamaria.lab4.ex2.Author;

public class TestBook {
	public static void main(String[] args) {
        Author a = new Author("John Green", "jgreen@user.com", 'm');
        Book b = new Book("Finding Alaska", a, 30);
        Book b2 = new Book("Fault in our Stars", a, 28, 18);
        b.setQtyInStock(13);
        b2.setQtyInStock(24);
        System.out.println(b.getAuthor().toString() + ' ' + b.getName() + ' ' + b.getPrice() + ' ' + b.getQtyInStock());
        System.out.println(b2.getAuthor().toString() + ' ' + b2.getName() + ' ' + b2.getPrice() + ' ' + b2.getQtyInStock());
    }
}
