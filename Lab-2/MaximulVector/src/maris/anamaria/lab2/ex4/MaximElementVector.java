package maris.anamaria.lab2.ex4;
import java.util.Collections;
import java.util.Vector;

public class MaximElementVector {
			
	public static void main(String[] args) {

	            Vector<Double> v = new Vector<Double>();

	            v.add(new Double("1.4324"));
	            v.add(new Double("3.549"));
	            v.add(new Double("7.342"));
	            v.add(new Double("6.349"));
	            v.add(new Double("2.3"));

	            Object obj = Collections.max(v);
	            System.out.println(obj);
	        }
	    

	}

