package maris.anamaria.lab2.ex5;
import java.util.Scanner;
public class Nerecursiv {
	public static void main(String args[]){
        int i,fact=1;
        Scanner  num = new Scanner(System.in);
        System.out.println("Which is the number?");
        int N = num.nextInt();
        for(i=1;i<=N;i++){
            fact=fact*i;
        }
        System.out.println("Factorial of "+N+" is: "+fact);
}
}