package maris.anamaria.lab7.ex3;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader("D:\\Facultate\\UT\\An 2\\SEM 2\\ISP\\maris-anamaria-g30123-isp-2019\\Lab-7\\Laboratotul 7\\src\\maris\\anamaria\\lab7\\ex3\\MesajImportant.dec"));
        BufferedWriter out = new BufferedWriter(new FileWriter("D:\\Facultate\\UT\\An 2\\SEM 2\\ISP\\maris-anamaria-g30123-isp-2019\\Lab-7\\Laboratotul 7\\src\\maris\\anamaria\\lab7\\ex3\\MesajImportant.enc"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = in.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = in.readLine();
            }
            String fileString = sb.toString();
            int n=fileString.length();
            char[] fileChars=fileString.toCharArray();
            for(int i=0;i<n;i++)
            {
                fileChars[i]<<=1;
            }
            String encripted= new String(fileChars);
            out.write(encripted);
        } finally {
            in.close();
            out.close();
        }
    }
}
