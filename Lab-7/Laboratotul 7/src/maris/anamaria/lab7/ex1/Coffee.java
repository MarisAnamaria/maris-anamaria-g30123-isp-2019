package maris.anamaria.lab7.ex1;

public class Coffee {
	private int temp;
    private int conc;
    static int numberCoffee = 0;

    Coffee(int t,int c){temp = t;conc = c;numberCoffee++;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    static int getNumberCoffee() {return numberCoffee;}
    public String toString(){return "[coffee temperature="+temp+":concentration="+conc+"]";}
}
