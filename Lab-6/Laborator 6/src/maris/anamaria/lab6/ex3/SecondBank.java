package maris.anamaria.lab6.ex3;

import java.util.Set;
import java.util.TreeSet;

public class SecondBank {

	    Set<SecondBankAccount> accounts = new TreeSet<SecondBankAccount>().descendingSet();


	    public void addAccount(String owner, double balance) {
	        accounts.add(new SecondBankAccount(owner, balance));
	    }

	    public void printAccounts() {
	        for (SecondBankAccount acc : accounts) {
	            System.out.println(acc.toString());
	        }
	    }

	    public void printAccounts(double minBalance, double maxBalance) {
	        for (SecondBankAccount acc : accounts) {
	            if (acc.getBalance() >= minBalance && acc.getBalance() <= maxBalance)
	                System.out.println(acc.toString());
	        }
	    }

	    public Set<SecondBankAccount> getAllAccounts() {
	        return accounts;
	    }

	}

