package maris.anamaria.lab6.ex3;

public class SecondBankAccount implements Comparable<SecondBankAccount> {

    private String owner;
    private double balance;

    public SecondBankAccount(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String toString() {
        return this.owner + " " + this.balance;
    }

    @Override
    public int compareTo(SecondBankAccount acc) {
        if (Double.valueOf(this.getBalance()) == Double.valueOf(acc.getBalance())) return 0;
        else if (Double.valueOf(this.getBalance()) > Double.valueOf(acc.getBalance())) return 1;
        else return -1;
    }

}
