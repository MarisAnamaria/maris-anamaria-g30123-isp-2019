package maris.anamaria.lab6.ex3;

public class Test {
	public static void main(String[] args) {
        SecondBank bank2 = new SecondBank();
        bank2.addAccount("Maria", 2138);
        bank2.addAccount("Tudor", 41);
        bank2.addAccount("Ilie", 2428);
        bank2.addAccount("Corina", 632);
        bank2.addAccount("Ana", 42);

        bank2.printAccounts();
    }
}
