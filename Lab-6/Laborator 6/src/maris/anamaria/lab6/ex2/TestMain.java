package maris.anamaria.lab6.ex2;
import maris.anamaria.lab6.ex1.BankAccount;

public class TestMain {
    public static void main(String[] args) {
        Bank bankAccount=new Bank();
        bankAccount.addAccount("Crina",30312);
        bankAccount.addAccount("Irina",310);
        bankAccount.addAccount("Bogdi",320);
        bankAccount.addAccount("Cata",414214);
        bankAccount.addAccount("Tudor",1231);
        bankAccount.addAccount("Andrei",4124);
        bankAccount.addAccount("Alex",42143);
        bankAccount.addAccount("Ionut",231);

        bankAccount.printAccounts();
        System.out.println("------------------------------------");
        bankAccount.printAccounts(1000,4000);
        System.out.println("------------------------------------");
        BankAccount ovi=bankAccount.getAccount("Crina");
        System.out.println("------------------------------------");
        System.out.println(ovi.toString());
        System.out.println("------------------------------------");

    }
}

